<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCart extends Model
{
    protected $table = 'products_cart';

    public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }
    
    public function productVariant()
    {
        return $this->belongsTo('App\ProductVariant','product_variant_id');
    }
}
