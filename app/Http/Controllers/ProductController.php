<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Product,ProductVariant,ProductCart};

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with(['images','variants'])->paginate(8);
        $cartCount = ProductCart::get()->count();
        
        return view('products',compact('products','cartCount'));
    }
    
    public function productDetails($id)
    {
        $product = Product::where('id',$id)
        ->with(['images','variants'])
        ->first();

        $seletedProducts = Product::where('id','!=',$id)
        ->with(['images','variants'])
        ->get()
        ->random(4);

        return view('product-details', compact('product','seletedProducts'));
    }

    public function addItemToCart(Request $request)
    {
        if($request->product_id && $request->size && $request->color && $request->quantity){
            
            session()->forget('combination');
            $existedVariant = ProductVariant::where('product_id',$request->product_id)
            ->where('size',$request->size)
            ->where('color',$request->color)
            ->first();
            if(!$existedVariant){
                \Session::put('combination','Color or size combination is not available.');
                
                return redirect()->back();
            }
            
            $existedCart = ProductCart::where('product_id',$request->product_id)
            ->where('product_variant_id',$existedVariant->id)
            ->first();
            
            if($request->product_id && Product::where('id',$request->product_id)->exists() && $existedVariant && !$existedCart){
                
                $cart = new ProductCart();
                $cart->product_id           = $request->product_id;
                $cart->product_variant_id   = $existedVariant->id;
                $cart->quantity             = $request->quantity;
                if($cart->save()){
                    \Session::put('success','Item added successfully.');
                } else {
                    
                    \Session::put('success','Item not added, please try later.');
                }
            } else {
                
                \Session::put('success','Item not added, please try later.');
            }
        
        }
        $products = ProductCart::with(['product'=>function($query){
            $query->with('images');
        },'productVariant'])
        ->get(); 

        return view('product-cart', compact('products'));
    }
    
    public function removeItemFromCart($id)
    {
        if(ProductCart::where('id',$id)->delete()) {
            // \Session::put('cart_message',"Cart item deleted successfully.");

            return redirect()->route('addItemToCart');
        } 
        // \Session::put('cart_message',"Cart item not deleted, please try later.");
        
        return redirect()->route('addItemToCart');
    }
}
