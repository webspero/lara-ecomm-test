<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function images()
    {
        return $this->hasMany('App\ProductImage','product_id');
    }
    
    public function variants()
    {
        return $this->hasMany('App\ProductVariant','product_id');
    }
}
