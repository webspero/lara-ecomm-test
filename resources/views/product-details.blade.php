<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Product Detail</title>
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- Css Styles -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/elegant-icons.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/slicknav.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css"> </head>
    <link rel="stylesheet" href="css/style.css" type="text/css"> </head>

<body>
    <!-- Page Preloder -->
    <!-- <div id="preloder">
        <div class="loader"></div>
    </div> -->
    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links"> 
                        <a href="{{ route('product.index') }}"><i class="fa fa-home"></i> Home</a> <span>Shirt</span> 
                        <a href="{{route('addItemToCart')}}" class=" pull-right" id="cart-count-button">{{-- $cartCount --}} Cart</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <!-- Product Details Section Begin -->
    <section class="product-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="product__details__pic">
                        <div class="product__details__pic__left product__thumb nice-scroll">
                            <a class="pt active" href="#product-1"> <img src="{{asset('storage/shop/'.$product->images[0]->path??'')}}" alt=""> </a>
                            <a class="pt" href="#product-2"> <img src="{{asset('storage/product/details/thumb-2.jpg')}}" alt=""> </a>
                            <a class="pt" href="#product-3"> <img src="{{asset('storage/product/details/thumb-3.jpg')}}" alt=""> </a>
                            <a class="pt" href="#product-4"> <img src="{{asset('storage/product/details/thumb-4.jpg')}}" alt=""> </a>
                        </div>
                        <div class="product__details__slider__content">
                            <div class="product__details__pic__slider owl-carousel"> 
                                <img data-hash="product-1" class="product__big__img" src="{{asset('storage/shop/'.$product->images[0]->path??'')}}" alt=""> 
                                <img data-hash="product-2" class="product__big__img" src="{{asset('storage/product/details/product-3.jpg')}}" alt=""> 
                                <img data-hash="product-3" class="product__big__img" src="{{asset('storage/product/details/product-2.jpg')}}" alt=""> 
                                <img data-hash="product-4" class="product__big__img" src="{{asset('storage/product/details/product-4.jpg')}}" alt=""> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <form method="get" action="{{route('addItemToCart')}}">
                    {!! csrf_field() !!}
                    <input type="hidden" value="{{ $product->id }}" name="product_id">
                    <div class="product__details__text">
                        <h3>{{ $product->name??"" }} <span>Brand: {{ $product->brand??"" }}</span></h3>
                        <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <span>( 138 reviews )</span> </div>
                        <div class="product__details__price">$ {{ $product->price??"" }} </div>
                        <p>{{ $product->description??"" }}</p>
                        <div class="product__details__button">
                            <div class="quantity"> <span>Quantity:</span>
                                <div class="pro-qty">
                                    <input type="text" value="1" name="quantity"> 
                                </div>
                            </div> <button type="submit" class="cart-btn"><span class="icon_bag_alt"></span> Add to cart</button>
                            <ul>
                                <li><a href="#"><span class="icon_heart_alt"></span></a></li>
                            </ul>
                        </div>
                        <div class="product__details__widget">
                            <ul>
                                <li> <span>Availability:</span>
                                    <div class="stock__checkbox">
                                        <label for="stockin"> In Stock
                                            <input type="checkbox" id="stockin" disabled {{ $product->in_stock ? 'checked' : '' }}> <span class="checkmark"></span> </label>
                                    </div>
                                </li>
                                <li> <span>Available color:</span>
                                    <div class="color__checkbox">
                                        @foreach($product->variants as $variant)
                                        <label for="{{ $variant->color }}">
                                            <input type="radio" value="{{ $variant->color }}" name="color" id="{{ $variant->color }}" checked> <span class="checkmark" style="background:{{ $variant->color??'' }}"></span> 
                                        </label>
                                        @endforeach
                                    </div>
                                </li>
                                <li> <span>Available size:</span>
                                    <div class="size__btn">
                                    <div class="select-box">
                                        <label for="select-box1" class="label select-box1">
                                            <span class="label-desc">Size</span> 
                                        </label>
                                        <select id="select-box1" class="select" name="size">
                                            @foreach($product->variants as $variant)
                                                <option value="{{$variant->size??''}}" >{{$variant->size??""}}</option>
                                            @endforeach
                                            </select>
                                   
                                </li>
                                <li style="color:red">
                                    {{ session('combination')??"" }}
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
                </div>
                <div class="col-lg-12">
                    <div class="product__details__tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Description</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Specification</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Reviews ( 2 )</a> </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <h6>Description</h6>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut loret fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt loret. Neque porro lorem quisquam est, qui dolorem ipsum quia dolor si. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut loret fugit, sed quia ipsu consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Nulla consequat massa quis enim.</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>
                            </div>
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <h6>Specification</h6>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut loret fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt loret. Neque porro lorem quisquam est, qui dolorem ipsum quia dolor si. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut loret fugit, sed quia ipsu consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Nulla consequat massa quis enim.</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <h6>Reviews ( 2 )</h6>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut loret fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt loret. Neque porro lorem quisquam est, qui dolorem ipsum quia dolor si. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut loret fugit, sed quia ipsu consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Nulla consequat massa quis enim.</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="related__title">
                        <h5>RELATED PRODUCTS</h5> </div>
                </div>
                @foreach($seletedProducts as $seletedProduct)
                <div class="col-lg-3 col-md-6">
                    <div class="product__item sale">
                        <div class="product__item__pic set-bg" onclick="window.location='{{ route('productDetails', ['id' => $seletedProduct->id]) }}'"  data-setbg="{{asset('storage/shop/'.$seletedProduct->images[0]->path)}}">
                            <div class="label">Sale</div>
                            <ul class="product__hover">
                                <li><a href="{{asset('storage/shop/'.$seletedProduct->images[0]->path)}}" class="image-popup"><span class="arrow_expand"></span></a></li>
                                <li><a href="#"><span class="icon_heart_alt"></span></a></li>
                                <li><a href="{{ route('productDetails', ['id' => $seletedProduct->id]) }}"><span class="icon_bag_alt"></span></a></li>
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="{{ route('productDetails', ['id' => $seletedProduct->id]) }}">{{ $seletedProduct->name??"" }}</a></h6>
                            <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                            <div class="product__price">$ {{ $seletedProduct->price??"" }}</div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->
    <!-- Js Plugins -->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>

    <script>
    $(document).ready(function(){

        $(".product__details__tab").val("{{ $product->variant->size??'' }}").change();
    })
        $("select").on("click", function () {
            $(this).parent(".select-box").toggleClass("open");
        });
        $(document).mouseup(function (e) {
            var container = $(".select-box");
            if (container.has(e.target).length === 0) {
                container.removeClass("open");
            }
        });
        $("select").on("change", function () {
            var selection = $(this).find("option:selected").text()
                , labelFor = $(this).attr("id")
                , label = $("[for='" + labelFor + "']");
            label.find(".label-desc").html(selection);
        });
        // Resources1× 0.5× 0.25× Rerun
    </script>
</body>

</html>