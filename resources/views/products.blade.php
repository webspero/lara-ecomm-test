<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">


        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Products</title>
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- Css Styles -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/elegant-icons.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/slicknav.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css"> </head>

        <!-- Styles -->
        <style>
            
        </style>
    </head>
    <body>
    <!-- <div id="preloder">
        <div class="loader"></div>
    </div> -->
    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links"> 
                        <a href="{{ route('product.index') }}"><i class="fa fa-home"></i> Home</a> <span>Shop</span> 
                        <a href="{{route('addItemToCart')}}" class=" pull-right" id="cart-count-button">{{-- $cartCount --}} Cart</a>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <!-- Shop Section Begin -->
    <section class="shop spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="row">
                    @forelse($products as $product)
                        <div class="col-lg-3 col-md-6">
                            <div class="product__item {{ $product->in_stock ? '' : 'out-of-stock' }}">
                                <div class="product__item__pic set-bg" onclick="window.location='{{ route('productDetails', ['id' => $product->id]) }}'"  data-setbg="{{asset('storage/shop/'.$product->images['0']->path) }}">
                                    <div class="label {{ $product->in_stock ? 'new' : 'stockout stockblue' }}">
                                        {{ $product->in_stock ? 'New' : 'Out Of Stock' }}
                                    </div>
                                    <ul class="product__hover">
                                        <li><a href="{{asset('storage/shop/shop-1.jpg') }}" class="image-popup"><span class="arrow_expand"></span></a></li>
                                        <li><a href="#"><span class="icon_heart_alt"></span></a></li>
                                        <li><a onclick="addToCart({{$product->id}} , {{$product->variants[0]->color}}, {{$product->variants[0]->size}} )"><span title="Add item" class="icon_bag_alt"></span></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6><a href="{{ route('productDetails', ['id' => $product->id]) }}">{{ $product->name??"" }}</a></h6>
                                    <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                                    <div class="product__price">$ {{ $product->price??"" }}</div>
                                </div>
                            </div>
                        </div>
                        @empty
                        <p>No Products</p>
                        
                        @endforelse
                        <div class="col-lg-12 text-center">
                            <div class=""> {{ $products->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shop Section End -->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
            
        </div>
    <script>
        function addToCart(id,color,size, e) {
             e.preventDefault();
            console.log(id);
            console.log(size);
            console.log(color);
            // var url = '{{route('productDetails')}}';
            // console.log(url);
            // $.ajax({
            //     url: url,
            //     id: id,
            //     success: function(html){
            //        console.log(html);
            //     }
            // });

        }
        $(document).ready(function(){

        });

    </script>

    </body>
</html>
