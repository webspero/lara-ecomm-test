<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ProductController@index')->name('product.index');

Route::get('products','ProductController@index')->name('product.index');
Route::get('product-details/{id?}','ProductController@productDetails')->name('productDetails');
Route::get('add-item','ProductController@addItemToCart')->name('addItemToCart');
Route::get('remove-item/{id?}','ProductController@removeItemFromCart')->name('removeItemFromCart');